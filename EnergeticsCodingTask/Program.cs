﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergeticsCodingTask
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = string.Empty;
            while (true)
            {
                Console.WriteLine("Enter desired value to convert: ");
                input = Console.ReadLine();
                if (input == "test")
                    RunExamplesSuite();
                else
                    Console.WriteLine($"It's {ConvertCurrencyToText(input)}");
            }
 
        }

        private static string ConvertCurrencyToText(string input)
        {
            // initialization of internal variables
            int dollars = 0;
            int cents = 0;

            // input sanitation and pre-processing
            if (input.IndexOf(',') >= 0) // checks if the input string contains a decimal point
            {
                var inputValueParts = input.Split(',');
                if (inputValueParts[1].Length > 2) return "an error (insert cents with up to 2 digits of precision and try again).";
                if (inputValueParts[1].Length == 1) inputValueParts[1] = $"{inputValueParts[1]}0"; // expands decimal part for cents
                
                if (!Int32.TryParse(inputValueParts[0].RemoveEverythingButDigits(), out dollars) || !Int32.TryParse(inputValueParts[1], out cents)) return "an error parsing the values for dollars and cents. Try again!";

                string outputString = dollars == 1 ? "one dollar and " : $"{dollars.ConvertToTextForm()} dollars and ";
                outputString += cents == 1 ? "one cent." : $"{cents.ConvertToTextForm()} cents.";
                return outputString;
            }
            else
            {
                if (Int32.TryParse(input.RemoveEverythingButDigits(), out dollars) == false) return "an error parsing the value for dollars. Try again!";
                return dollars == 1 ? "one dollar." : $"{dollars.ConvertToTextForm()} dollars.";
            }

        }

        private static void RunExamplesSuite()
        {
            Console.WriteLine("input: 0              output: " + ConvertCurrencyToText("0"));
            Console.WriteLine("input: 1              output: " + ConvertCurrencyToText("1"));
            Console.WriteLine("input: 25,1           output: " + ConvertCurrencyToText("25,1"));
            Console.WriteLine("input: 0,01           output: " + ConvertCurrencyToText("0,01"));
            Console.WriteLine("input: 45 100         output: " + ConvertCurrencyToText("45 100"));
            Console.WriteLine("input: 999 999 999,99 output: " + ConvertCurrencyToText("999 999 999,99"));
        }
    }
}
