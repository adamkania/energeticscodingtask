﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergeticsCodingTask
{
    public static class HelperMethods
    {
        public static string RemoveEverythingButDigits(this string input)
        {
            return String.Join(string.Empty, input.Where(x => Char.IsDigit(x)));
        }
        public static List<int> ToListOfDigits(this int input)
        {
            return input.ToString().Select(x => Int32.Parse(x.ToString())).ToList();
        }

        public static string ConvertToTextForm(this int input)
        {
            if (input == 0) return "zero";
            List<int> digits = input.ToListOfDigits();
            var output = new StringBuilder();

            while (digits.Count > 0)
            {
                // break early if the rest of the digits are just 0s
                if (digits.All(x => x == 0)) digits.Clear();
                // skip three digits if they're 0s (for consistency in big numbers like one billion one hundred (1000000100))
                if (digits.Count >= 3 && digits.GetRange(0,3).All(x => x == 0))
                {
                    digits.RemoveRange(0, 3);
                    continue;
                }


                switch (digits.Count)
                {
                    case 10:
                        output.Append($" {digits[0].From0to9()} billion");
                        digits.RemoveAt(0); break;
                    case 9:
                        output.Append($" {digits.GetRange(0, 3).ToArray().From0to999()} million");
                        digits.RemoveRange(0,3); break;
                    case 8:
                        output.Append($" {digits.GetRange(0, 2).ToArray().From0to99()} million");
                        digits.RemoveRange(0, 2); break;
                    case 7:
                        output.Append($" {digits[0].From0to9()} million");
                        digits.RemoveAt(0); break;
                    case 6:
                        output.Append($" {digits.GetRange(0, 3).ToArray().From0to999()} thousand");
                        digits.RemoveRange(0, 3); break;
                    case 5:
                        output.Append($" {digits.GetRange(0, 2).ToArray().From0to99()} thousand");
                        digits.RemoveRange(0, 2); break;
                    case 4:
                        output.Append($" {digits[0].From0to9()} thousand");
                        digits.RemoveAt(0); break;
                    case 3:
                        output.Append($" {digits.GetRange(0, 3).ToArray().From0to999()}");
                        digits.RemoveRange(0, 3); break;
                    case 2:
                        output.Append($" {digits.GetRange(0, 2).ToArray().From0to99()}");
                        digits.RemoveRange(0, 2); break;
                    case 1:
                        output.Append($" {digits[0].From0to9()}");
                        digits.RemoveAt(0); break;
                }
            }


            return output.ToString().Trim();


        }

        private static string From0to999(this int[] threeDigits)
        {
            var builder = new StringBuilder();

            if (threeDigits[0] != 0) builder.Append($"{threeDigits[0].From0to9()} hundred ");

            if (threeDigits[1] != 0)
            {
                builder.Append($"{threeDigits.Skip(1).Take(2).ToArray().From0to99()}");
                return builder.ToString();
            }
            if (threeDigits[2] != 0)
            {
                if (threeDigits[0] != 0 && threeDigits[2] != 0) builder.Append("and ");
                builder.Append($"{threeDigits[2].From0to9()}");
            }

            return builder.ToString().Trim();
        }

        private static string From0to99(this int[] twoDigits)
        {
            if (twoDigits[0] == 0)
            {
                return From0to9(twoDigits[1]);
            }
            else if (twoDigits[0] == 1)
            {
                switch (twoDigits[1])
                {
                    case 0: return "ten";
                    case 1: return "eleven";
                    case 2: return "twelve";
                    case 3: return "thirteen";
                    case 4: return "fourteen";
                    case 5: return "fifteen";
                    case 6: return "sixteen";
                    case 7: return "seventeen";
                    case 8: return "eighteen";
                    case 9: return "nineteen";
                }
            }
            else
            {
                var builder = new StringBuilder();
                // process first digit
                switch (twoDigits[0])
                {
                    case 9: builder.Append("ninety"); break;
                    case 8: builder.Append("eighty"); break;
                    case 7: builder.Append("seventy"); break;
                    case 6: builder.Append("sixty"); break;
                    case 5: builder.Append("fifty"); break;
                    case 4: builder.Append("fourty"); break;
                    case 3: builder.Append("thirty"); break;
                    case 2: builder.Append("twenty"); break;
                    default:
                        break;
                }
                // add dash and second digit in case of composite number
                if (twoDigits[1] != 0)
                {
                    builder.Append("-");
                    builder.Append(From0to9(twoDigits[1]));
                }

                return builder.ToString();
            }

            return "error";

        }

        private static string From0to9(this int digit)
        {
            switch (digit)
            {
                case 1: return "one";
                case 2: return "two";
                case 3: return "three";
                case 4: return "four";
                case 5: return "five";
                case 6: return "six";
                case 7: return "seven";
                case 8: return "eight";
                case 9: return "nine";
                case 0: return "zero";
                default:
                    return String.Empty;
            }
        }
    }
}
