﻿// NUnit 3 tests
// See documentation : https://github.com/nunit/docs/wiki/NUnit-Documentation
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using EnergeticsCodingTask;

namespace EnergeticsCodingTaskTests
{
    [TestFixture]
    public class HelperMethodsTest
    {
        [Test]
        public void ToListOfDigits()
        {
            Assert.That(HelperMethods.ToListOfDigits(1), Is.TypeOf<List<int>>(), "Returned type is incorrect.");
            Assert.That(HelperMethods.ToListOfDigits(576), Is.EqualTo(new List<int> { 5, 7, 6 }), "The expected result is incorrect.");
            Assert.That(HelperMethods.ToListOfDigits(954228), Is.EqualTo(new List<int> { 9, 5, 4, 2, 2, 8 }), "The expected result is incorrect.");
        }

        [Test]
        public void RemoveEverythingButDigits()
        {
            Assert.That(HelperMethods.RemoveEverythingButDigits("hello"), Is.TypeOf<string>(), "Returned type is incorrect.");
            Assert.That(HelperMethods.RemoveEverythingButDigits(string.Empty), Is.EqualTo(string.Empty), "The method doesn't properly handle empty strings.");
            Assert.That(HelperMethods.RemoveEverythingButDigits("642abc123"), Is.EqualTo("642123"), "The method incorrectly removes letters.");
            Assert.That(HelperMethods.RemoveEverythingButDigits("123 435    8"), Is.EqualTo("1234358"), "The method incorrectly removes whitespace.");
        }
        
        [Test]
        public void ConvertToTextForm()
        {
            Assert.That(HelperMethods.ConvertToTextForm(432), Is.TypeOf<string>(), "Returned type is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(0), Is.EqualTo("zero"), "The method doesn't handle zero correctly.");
            Assert.That(HelperMethods.ConvertToTextForm(1), Is.EqualTo("one"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(12), Is.EqualTo("twelve"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(59), Is.EqualTo("fifty-nine"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(100), Is.EqualTo("one hundred"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(101), Is.EqualTo("one hundred and one"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(137), Is.EqualTo("one hundred thirty-seven"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(455), Is.EqualTo("four hundred fifty-five"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(1000), Is.EqualTo("one thousand"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(1070), Is.EqualTo("one thousand seventy"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(26423), Is.EqualTo("twenty-six thousand four hundred twenty-three"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(156722), Is.EqualTo("one hundred fifty-six thousand seven hundred twenty-two"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(1000000), Is.EqualTo("one million"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(1001001), Is.EqualTo("one million one thousand one"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(200050000), Is.EqualTo("two hundred million fifty thousand"), "The expected result is incorrect.");
            Assert.That(HelperMethods.ConvertToTextForm(1000000070), Is.EqualTo("one billion seventy"), "The expected result is incorrect.");
        }
    }
}
